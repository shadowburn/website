---
layout: post
title: Introduction to Shadowburn
author: Friate
---

The goal of the Shadowburn Project is to make a high-performance, distributed, fault-tolerant core for vanilla private servers.

### Why a new core?

The majority of private servers running today use [MaNGOS](https://www.getmangos.eu/), or a fork of it, which is a very powerful core written using C++. While C++ is very fast, it can be difficult to track down issues with memory leaks, multi-threading can be complex, and the code in general can be difficult to follow.

Shadowburn Project is being written in [Elixir](https://elixir-lang.org), which is built on top of the Erlang VM. This provides a solid foundation for a dstributed and fault-tolerant private server that can handle a large number of players. After playing with Elixir, I believe it's a perfect fit for this problem.

This is something I've always been interested in and I believe the best way to learn about something is to do it. There's much to be said about building a new project from the ground up, and understanding everything that goes into it.

### Contribute
There is so much work to be done that I would be happy to accept any help. To contribute to the next generation of vanilla private servers, please contact me at [contribute@shadowburn-project.org](mailto:contribute@shadowburn-project.org) or [join the discord](https://discord.gg/bXgPxn2). I'm looking for people with an interest in functional programming, game development, and/or very early testing.